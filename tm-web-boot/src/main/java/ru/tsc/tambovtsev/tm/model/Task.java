package ru.tsc.tambovtsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners({AuditingEntityListener.class})
public class Task extends AbstractWbsModel{

    private static final long serialVersionUID = 2;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

    public Task(@NotNull final String name) {
        super(name);
    }

    public Task(@NotNull final String name, @Nullable final String description) {
        super(name, description);
    }

    public Task(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        super(name, status, dateBegin);
    }

    public Task(
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(name, description, dateBegin, dateEnd);
    }

}
