package ru.tsc.tambovtsev.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.Result;
import ru.tsc.tambovtsev.tm.model.User;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class UserRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String USER_URL = "http://localhost:8080/api/user/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private final User user = new User("User1", "efrg3434f3dwed2", "user1@us.com");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?login=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isValue());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void initTest() {
        @NotNull final String url = USER_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(user, header), User.class);
    }

    @After
    public void clean() {
        @NotNull final String url = USER_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(user, header), User.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = USER_URL + "count";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findByLoginTest() {
        @NotNull final String url = USER_URL + "findByLogin/" + user.getLogin();
        @NotNull final ResponseEntity<User> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), User.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final User userFind = response.getBody();
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getId(), userFind.getId());
    }

    @Test
    public void deleteTest() {
        clean();
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteByLoginTest() {
        @NotNull final String url = USER_URL + "deleteByLogin/" + user.getLogin();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), User.class);
        Assert.assertEquals(2, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }
    
}
