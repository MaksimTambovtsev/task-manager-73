package ru.tsc.tambovtsev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${environment['buildNumber']:1.57}")
    public String applicationVersion;

    @Value("${environment['email']:mtambovtsev@t1-consulting.ru}")
    public String authorEmail;

    @Value("${environment['developer']:Maksim Tambovtsev}")
    public String authorName;

    @Value("${environment['server.port']:8080}")
    public String port;

    @Value("${environment['server.host']:localhost}")
    private String host;

    @Value("${environment['password.iteration']:25456}")
    public String passwordIteration;

    @Value("${environment['password.secret']:356585985}")
    public String passwordSecret;

}
