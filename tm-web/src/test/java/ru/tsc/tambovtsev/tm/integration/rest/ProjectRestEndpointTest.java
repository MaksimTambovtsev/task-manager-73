package ru.tsc.tambovtsev.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Result;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private final Project project = new Project("Project1", "Description1");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?login=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isValue());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void initTest() {
        @NotNull final String url = PROJECT_URL + "save/";
        project.setUserId(UserUtil.getUserId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project, header), Project.class);
    }

    @After
    public void clean() {
        @NotNull final String url = PROJECT_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Project.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = PROJECT_URL + "count";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = PROJECT_URL + "findAll/";
        @NotNull final ResponseEntity<List> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), List.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final List projectList = response.getBody();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = PROJECT_URL + "findById/" + project.getId();
        @NotNull final ResponseEntity<Project> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Project project2 = response.getBody();
        Assert.assertNotNull(project2);
        Assert.assertEquals(project.getId(), project2.getId());
    }

    @Test
    public void saveTest() {
        @NotNull final String url = PROJECT_URL + "save/";
        @NotNull final Project project2 = new Project("Project2", "Description2");
        @NotNull final ResponseEntity<Project> response =
                sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Project projectResult = response.getBody();
        Assert.assertNotNull(projectResult);
        Assert.assertEquals(projectResult.getId(), project2.getId());
    }

    @Test
    public void deleteTest() {
        @NotNull final String url = PROJECT_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(project, header), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteAllTest() {
        @NotNull final String url = PROJECT_URL + "deleteAll/";
        sendRequest(
                url,
                HttpMethod.DELETE,
                new HttpEntity<>(Collections.singletonList(project), header),
                Project.class
        );
        Assert.assertEquals(0, count());
    }

    @Test
    public void clearTest() {
        @NotNull final String url = PROJECT_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String url = PROJECT_URL + "deleteById/" + project.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}
