package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    @Transactional
    Task create(@Nullable String userId);

    @NotNull
    @Transactional
    Task create(@Nullable String userId, @NotNull String name);

    @NotNull
    @Transactional
    Task save(@NotNull Task task);

    @Nullable
    Collection<Task> findAll(@Nullable String userId);

    @Nullable
    Task findById(@Nullable String userId, @NotNull String id);

    @Transactional
    void removeById(@Nullable String userId, @NotNull String id);

    @Transactional
    void remove(@NotNull Task task);

    @Transactional
    void remove(@NotNull List<Task> tasks);

    @Transactional
    void removeByUserId(@Nullable final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@Nullable String userId, @NotNull String id);

    @Transactional
    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
