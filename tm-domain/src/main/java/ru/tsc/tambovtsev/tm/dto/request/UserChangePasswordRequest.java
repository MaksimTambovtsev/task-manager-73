package ru.tsc.tambovtsev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable String token, @Nullable String password) {
        super(token);
        this.password = password;
    }

}
